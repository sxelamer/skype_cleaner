import datetime
import platform


system = platform.system().lower()


def ts_to_human_date(ts, format='%Y-%m-%d %H:%M:%S'):
    return datetime.datetime.fromtimestamp(int(ts)).strftime(format)