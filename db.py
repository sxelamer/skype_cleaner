from peewee import SqliteDatabase, Model, Proxy, TextField, IntegerField, ForeignKeyField, PrimaryKeyField


database_proxy = Proxy()


class BaseModel(Model):

    class Meta:
        database = database_proxy


class Conversations(BaseModel):

    id = PrimaryKeyField()
    type = IntegerField()
    displayname = TextField()
    given_displayname = TextField()
    last_activity_timestamp = IntegerField()


class Chats(BaseModel):
    # TODO don't need it actually

    id = IntegerField()
    name = TextField()
    friendlyname = TextField()
    timestamp = IntegerField()
    conv_dbid = ForeignKeyField(Conversations, db_column='conv_dbid', related_name='conversations')


class Messages(BaseModel):

    id = PrimaryKeyField()
    author = TextField()
    from_dispname = TextField()
    body_xml = TextField()
    chatname = TextField()
    timestamp = IntegerField()
    convo_id = ForeignKeyField(Conversations, db_column='convo_id', related_name='messages')


def configure_db(db_path):
    database = SqliteDatabase(db_path, threadlocals=True)
    database_proxy.initialize(database)
