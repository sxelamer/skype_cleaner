"""
select Chats.friendlyname from Messages
join Chats on (Messages.chatname = Chats.name)

select Chats.friendlyname from Chats order by Chats.timestamp DESC
"""

import os
import sys

from peewee import DeleteQuery, fn, JOIN_LEFT_OUTER

from utils import system
from db import configure_db, Chats, Conversations, Messages


SKYPE_DB_NAME = 'main.db'


def _get_app_data_dir(app_name):
    if system == 'darwin':
        dir = os.path.expanduser('~/Library/Application Support/')
    elif system == 'windows':
        dir = os.path.expandvars('%appdata%')
    else:
        raise SystemError('System "%s" is not supported!' % system)

    return os.path.join(dir, app_name)


def _get_main_db_path(username):
    user_dir = os.path.join(_get_app_data_dir('skype'), username)
    main_db_path = os.path.join(user_dir, SKYPE_DB_NAME)
    return main_db_path


def skype_accounts_list():
    skype_dir = _get_app_data_dir('skype')
    accounts = []
    for name in os.listdir(skype_dir):
        account_dir = os.path.join(skype_dir, name)
        if os.path.isdir(account_dir) and os.path.isfile(os.path.join(account_dir, SKYPE_DB_NAME)):
            accounts.append(name)
    return accounts


class History(object):
    def __init__(self, account):
        db_path = _get_main_db_path(account)
        configure_db(db_path)


    def get_chats(self):
        # TODO don't need it
        chats = (Chats
                 .select()
                 .join(Conversations)
                 .order_by(Chats.timestamp))
        chats_list = []
        for chat in chats:
            print(chat)
            # chats_list.append((chat.id, chat.friendlyname))
        return chats_list


    def get_conversations(self):
        conversations = (Conversations
                         .select(Conversations, fn.Count(Messages.id).alias('messages_count'))
                         .join(Messages, JOIN_LEFT_OUTER)
                         .group_by(Conversations.id)
                         .order_by(Conversations.last_activity_timestamp.asc()))
        conv_list = []
        for conv in conversations:
            conv_name = conv.given_displayname if conv.given_displayname else conv.displayname
            messages_count = conv.messages_count
            if conv.type == 1 and conv.displayname.startswith('+'):
                # Filter mobile numbers. Type = 1 -> not group conversation,
                # so its name is real person's name which can't start with +
                continue
            conv_list.append({'conv_id': conv.id, 'conv_name': conv_name, 'messages_count': messages_count})
        return conv_list


    def get_messages_in_conversation(self, conversation_id):
        messages = (Messages
                    .select(Messages.id, Messages.author, Messages.from_dispname, Messages.body_xml, Messages.timestamp)
                    .join(Conversations)
                    .where(Messages.convo_id == conversation_id)
                    .order_by(Messages.timestamp.asc())
                    .dicts())
        messages_list = []
        for msg in messages:
            messages_list.append(msg)
        return messages_list


    def remove_messages(self, ids):
        query = DeleteQuery(Messages).where(Messages.id << ids)
        return query.execute()


    def update_message(self, msg_id, text):
        query = Messages.update(body_xml=text).where(Messages.id == msg_id)
        return query.execute()


if __name__ == '__main__':
    import datetime

    print skype_accounts_list()
    history = History('ckpumep')

    for i in history.get_conversations():
        print i
        break

    for i in history.get_messages_in_conversation(conversation_id=368):
        print '%s (%s): %s' % (i['from_dispname'],
                               datetime.datetime.fromtimestamp(i['timestamp']).strftime('%Y-%m-%d %H:%M:%S'),
                               i['body_xml'])
        break

