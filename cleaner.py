import sys
import ctypes

import skype
import utils

from peewee import OperationalError
from PySide.QtGui import *
from PySide.QtCore import *


if utils.system == 'windows':
    # register process to be able to show app's icon in taskbar rather than default icon
    myappid = 'mrgoodkat.skypecleaner.product.version' # arbitrary string
    ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)


def busy_cursor(fn):
    def wrapper(self, *args, **kwargs):
        QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))
        fn(self, *args, **kwargs)
        QApplication.restoreOverrideCursor()
    return wrapper


class SkypeCleaner(QWidget):

    def __init__(self):
        super(SkypeCleaner, self).__init__()

        self.initUI()


    def _set_remove_button_enabled(self, enabled=True):
        self.remove_btn.setEnabled(enabled)

    def initUI(self):

        self.account_lbl = QLabel('Account')
        self.account_lbl.setMinimumWidth(40)

        self.account = QComboBox()
        self.account.addItems(self._get_skype_accounts())
        self.account.setMinimumWidth(180)

        self.select_account_btn = QPushButton("Conversations", self)
        self.select_account_btn.clicked.connect(self.display_conversations)

        accounts_layout = QBoxLayout(QBoxLayout.LeftToRight)
        accounts_layout.addWidget(self.account_lbl)
        accounts_layout.addWidget(self.account)
        accounts_layout.addWidget(self.select_account_btn)

        self.hide_empty_checkbox = QCheckBox("Hide empty conversations")
        self.hide_empty_checkbox.setChecked(True)
        self.hide_empty_checkbox.setDisabled(True)
        self.hide_empty_checkbox.stateChanged.connect(self.display_conversations)

        self.conversations_list = QTableView()
        self.conversations_list.setEditTriggers(QAbstractItemView.DoubleClicked)
        self.conversations_list.setSelectionBehavior(QAbstractItemView.SelectRows)

        self.model = QStandardItemModel(self.conversations_list)
        self.model.dataChanged.connect(self.update_message)

        self.proxyModel =  QSortFilterProxyModel()
        self.proxyModel.setSourceModel(self.model)
        self.proxyModel.setFilterCaseSensitivity(Qt.CaseInsensitive)

        self.conversations_list.setModel(self.proxyModel)
        self.set_list_placeholder("Choose account to display conversations")
        self.conversations_list.horizontalHeader().hide()
        self.conversations_list.horizontalHeader().setResizeMode(QHeaderView.Stretch)
        self.conversations_list.verticalHeader().hide()
        self.conversations_list.resizeColumnsToContents()

        self.search_field = QLineEdit()
        self.search_field.setPlaceholderText("Search...")
        self.search_field.setDisabled(True)

        self.remove_btn = QPushButton("Remove")
        self.remove_btn.setMaximumWidth(100)
        self.remove_btn.clicked.connect(self.remove_messages)
        self._set_remove_button_enabled(False)

        self.bottom_buttons_layout = QHBoxLayout()
        self.bottom_buttons_layout.addWidget(self.remove_btn)

        self.main_layout = QGridLayout()

        self.main_layout.addLayout(accounts_layout, 0, 0)
        self.main_layout.addWidget(self.hide_empty_checkbox, 1, 0)
        self.main_layout.addWidget(self.conversations_list, 2, 0)
        self.main_layout.addWidget(self.search_field, 3, 0)
        self.main_layout.addLayout(self.bottom_buttons_layout, 4, 0)

        self.setLayout(self.main_layout)

        self.setGeometry(300, 200, 450, 500)
        self.setWindowTitle('Skype history cleaner')


    def set_list_placeholder(self, text):
        list_placeholder = QStandardItem(text)
        list_placeholder.setTextAlignment(Qt.AlignCenter)  # set alignment to center
        list_placeholder.setFlags(True)  # disable item
        self.model.appendRow(list_placeholder)
        self._hide_row_borders()


    def _hide_row_borders(self):
        self.conversations_list.setGridStyle(Qt.NoPen)


    def _show_row_borders(self):
        self.conversations_list.setGridStyle(Qt.SolidLine)


    def _conversations_list_handler(self):
        self._clear_search_field()
        self.proxyModel.setFilterKeyColumn(0)
        self.search_field.textChanged.connect(self._conversations_filter_handler)

        self.conversations_list.doubleClicked.connect(self.show_messages_in_conversation)
        selection_model = self.conversations_list.selectionModel()
        try:
            self.conversations_list.doubleClicked.disconnect(self._trigger_message_column_edit)
            selection_model.selectionChanged.disconnect(self._messages_selection_changed)
        except RuntimeError:
            print "Can not disconnect signal selectionChanged from selectionModel. But that's kind of ok."


    def _messages_list_handler(self):
        self._clear_search_field()
        self.proxyModel.setFilterKeyColumn(1)
        self.search_field.textChanged.connect(self._messages_filter_handler)

        self.conversations_list.doubleClicked.disconnect()
        self.conversations_list.doubleClicked.connect(self._trigger_message_column_edit)

    def _get_skype_accounts(self):
        accounts = skype.skype_accounts_list()
        return accounts


    def _messages_selection_changed(self, selected, deselected):
        selection_model = self.conversations_list.selectionModel()

        enabled = selection_model.hasSelection()
        self._set_remove_button_enabled(enabled)


    def _select_entire_row(self):
        print self.conversations_list.currentIndex()


    def remove_messages(self):
        dialog = QMessageBox(
            QMessageBox.Warning, "Remove selected messages",
            "You sure you want to remove selected messages? Operation can not be rolled back!",
        )
        dialog.setStandardButtons(QMessageBox.Yes | QMessageBox.Cancel)
        dialog.setDefaultButton(QMessageBox.Yes)
        choice = dialog.exec_()

        if choice == QMessageBox.Cancel:
            return

        selection_model = self.conversations_list.selectionModel()
        indexes = selection_model.selectedRows()

        removed_rows = []
        messages_ids = []
        for index in indexes:
            index = self.proxyModel.mapToSource(index)
            row = index.row()
            if row in removed_rows:
                continue
            selected_item = self.model.item(row, 1)
            messages_ids.append(selected_item.msg_id)
            self.model.removeRow(row)

            font = QFont(italic=True)
            brush = QBrush(Qt.darkGray)

            empty_item = QStandardItem()
            empty_item.setEditable(False)
            empty_item.setSelectable(False)
            dummy_item = QStandardItem("*** Message removed ***")
            dummy_item.setEditable(False)
            dummy_item.setSelectable(False)
            dummy_item.setFont(font)
            dummy_item.setForeground(brush)

            self.model.insertRow(row, empty_item)
            self.model.setItem(row, 1, dummy_item)
            self.proxyModel.invalidateFilter()

            removed_rows.append(row)

        self.skype.remove_messages(messages_ids)


    def refresh_conversations_list(self):
        self.model.clear()


    def set_list_multi_selection(self, enable=True):
        if enable:
            mode = QAbstractItemView.ExtendedSelection
        else:
            mode = QAbstractItemView.SingleSelection
        self.conversations_list.setSelectionMode(mode)


    @busy_cursor
    def display_conversations(self, state=None):
        selected_acc = self.account.currentText()
        self.skype = skype.History(selected_acc)

        self.refresh_conversations_list()
        self.search_field.setDisabled(False)
        self.set_list_multi_selection(False)
        self.hide_empty_checkbox.show()
        self.hide_empty_checkbox.setDisabled(False)
        self._set_remove_button_enabled(False)

        try:
            conversations = self.skype.get_conversations()
        except OperationalError:
            message = QMessageBox(QMessageBox.Critical, "Account in use",
                                        "Can't open history for selected account. Close Skype and try again.")
            return message.exec_()

        if not conversations:
            self.set_list_placeholder("No conversation to display.")
            return

        self._show_row_borders()
        self._conversations_list_handler()
        row = 0
        need_hide = self.hide_empty_checkbox.isChecked()
        for c in conversations:
            conv_item = QStandardItem('%s (%s messages)' % (c['conv_name'], c['messages_count']))
            conv_item.conv_id = c['conv_id']
            conv_item.messages_count = c['messages_count']
            conv_item.setEditable(False)
            self.model.appendRow(conv_item)

            if not need_hide:
                continue

            if not c['messages_count']:
                self.conversations_list.hideRow(row)

            row += 1
        self.conversations_list.scrollToBottom()


    @busy_cursor
    def show_messages_in_conversation(self, index):
        index = self.proxyModel.mapToSource(index)
        row = index.row()
        item = self.model.item(row)
        conversation_id = item.conv_id
        self._messages_list_handler()
        self.refresh_conversations_list()
        self.set_list_multi_selection(True)
        self.hide_empty_checkbox.hide()

        messages = self.skype.get_messages_in_conversation(conversation_id)
        if not messages:
            self.set_list_placeholder("Conversation is empty")
            return
        if len(messages) == 1 and not messages[0]['body_xml']:
            self.set_list_placeholder("Conversation is empty")
            return
        for msg in messages:
            msg_body = 'Message removed.' if not msg['body_xml'] else msg['body_xml']

            author_tooltip = '%s, %s' % (msg['author'], utils.ts_to_human_date(msg['timestamp']))
            author_item = QStandardItem(msg['from_dispname'])
            author_item.setEditable(False)
            author_item.setToolTip(author_tooltip)
            author_item.msg_id = msg['id']

            text_tooltip = msg_body
            text_item = QStandardItem(msg_body)
            text_item.setToolTip(text_tooltip)
            text_item.msg_id = msg['id']

            self.model.appendRow([author_item, text_item])

        header = QHeaderView(Qt.Horizontal)
        header.setStretchLastSection(True)
        self.conversations_list.setHorizontalHeader(header)
        self.conversations_list.setVerticalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.conversations_list.resizeColumnsToContents()
        self.conversations_list.scrollToBottom()

        selection_model = self.conversations_list.selectionModel()
        selection_model.selectionChanged.connect(self._messages_selection_changed)


    def update_message(self, index1, index2):
        item = self.model.itemFromIndex(index1)
        if not hasattr(item, 'msg_id'):
            # either removed item and replaced with some dummy text or bug. anyways can not remove without id.
            return
        self.skype.update_message(item.msg_id, item.text())


    def _trigger_message_column_edit(self, index):
        if index.column() == 1:
            return

        index = self.proxyModel.mapToSource(index)  # Get index in model
        row = index.row()
        item = self.model.item(row, 1)
        index = self.proxyModel.mapFromSource(item.index())  # Convert back index from source
        self.conversations_list.setCurrentIndex(index)
        self.conversations_list.edit(index)


    def _clear_search_field(self):
        self.search_field.clear()


    def _conversations_filter_handler(self):
        q = self.search_field.text()
        self.proxyModel.setFilterRegExp("^%(q)s.*|\\s%(q)s" % {'q': q})


    def _messages_filter_handler(self):
        q = self.search_field.text()
        self.proxyModel.setFilterRegExp(".*%s.*|\*\*\* Message removed \*\*\*" % q)

        if not len(q):
            selection_model = self.conversations_list.selectionModel()
            selected_indexes = selection_model.selectedRows()

            if selected_indexes:
                current_index = selected_indexes[-1]
                self.conversations_list.setCurrentIndex(current_index)
                self.conversations_list.scrollTo(current_index)


def main():

    app = QApplication(sys.argv)
    sc = SkypeCleaner()
    sc.show()
    sc.activateWindow()
    sc.setWindowIcon(QIcon('icon.ico'))
    sc.raise_()  # bring window to top
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()