# README #

Utility allows user to selective remove messages from Skype history.

### Requirements ###

* Qt 4.8
* python 2.7 

### Set up ###

* Install and activate virtualenv (optional)
* pip install requirements.txt

### Build binary ###

* **Mac OS:**

      python setup.py py2app